#include <iostream>
#include <string>
#include <string.h>
#include <list>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

#include "Arquivo_invertido.hpp"

#define ALFABETO 27

struct dados
{
	int arquivo, posicao, linha;
};

struct no
{
	string palavra;
	long ocorrencia;
	list<Dados*> dado;
	no *esq, *dir;
};

struct hash
{
	no *arvore;
};


no* iniciar_arvore()
{
	return NULL;
}

no* no_cria(string p)
{
	no* aux;
	aux = new no;

	aux->palavra = p;
	aux->ocorrencia = 1;
	aux->esq = NULL;
	aux->dir = NULL;
	aux->dado.clear();

	return aux;
}

dados* iniciar_dados (const int &linha, const int &posicao, const int &arquivo)
{
	dados *aux;
	aux = new dados;

	aux->linha = linha;
	aux->posicao = posicao;
	aux->arquivo = arquivo;

	return aux;
}

void inserir (no **raiz, const string &palavra, Dados *dado, long &cont)
{
	cont++;
	if((*raiz) == NULL)
	{
		*raiz = no_cria(palavra);
		(*raiz)->dado.push_back(dado);
		return;
	}
	cont++;
	if(palavra == (*raiz)->palavra)
	{
		(*raiz)->ocorrencia++;
		(*raiz)->dado.push_back(dado);
		return;
	}
	cont++;
	if(palavra < (*raiz)->palavra)
	{
		inserir(&((*raiz)->esq), palavra, dado, cont);
		return;
	}
	else
	{
		inserir(&((*raiz)->dir), palavra, dado, cont);
		return;	
	}
}

void imprimir(list<dados*>& lista)
{
	int i = 1;
	for(list<dados*>::iterator it = lista.begin(); it != lista.end(); it++, i++)
		{
			
			cout << "\nOcorrencia numero "<< i <<endl;
			cout << "Documento = "<< (*it)->arquivo << endl;
			cout << "Linha     = "<< (*it)->linha << endl;
			cout << "Posicao   = "<< (*it)->posicao << endl;
		}
}

bool buscar (no **raiz, const string &palavra, long &comp)
{
	comp++;
	if((*raiz) == NULL)
		return false;
	
	comp++;
	if(palavra == (*raiz)->palavra)
	{
		cout << "\nPalavra: " << (*raiz)->palavra << "   Ocorrencias: " << (*raiz)->ocorrencia << endl;

		imprimir((*raiz)->dado);
		return true;
	}

	comp++;
	if(palavra < (*raiz)->palavra)
		return buscar(&((*raiz)->esq), palavra, comp);

	else
		return buscar(&((*raiz)->dir), palavra, comp);
}

bool buscar (no **raiz, list<Dados*> &d1, const string &p1, const string &p2, int comp) // busca 2 para duas palavras
{	
	comp++;
	if((*raiz) == NULL) // nao existe a palavra, logo nao existe a sequencia de p1 e p2 tambem
		return false;
	comp++;
	if((*raiz)->palavra == p2) // achou a segunda palavra
	{
		// para cada p1, busca umas p2 que esteja logo em seguida
		for(list<dados*>::iterator it = d1.begin(); it != d1.end(); it++) // percorre toda a lista de ocorrencia da peimeira palavra
		{
			int fim = (*it)->posicao + p1.size() + 1; // fim da primeira palavra = (tamanho dela + expaço em branco)
			for(list<dados*>::iterator it2 = (*raiz)->dado.begin(); it2 != (*raiz)->dado.end(); it2++) // percorre toda a lista de ocorrencia da segunda palavra
			{
				comp++;
				if((*it2)->arquivo > (*it)->arquivo) // a segunda palavra está algumas páginas na frente
					break; // evita busca sem necessidade
				comp++;
				if((*it2)->arquivo == (*it)->arquivo) // mesma pagina
				{
					comp++;
					if((*it2)->linha == (*it)->linha) // mesma linha
					{	
						comp++;
						if((*it2)->posicao == fim) // uma seguida da outra
						{
							cout << endl << "-----------------------------" << endl
								 << "Arquivo: " << (*it)->arquivo << endl
								 << "Linha: " << (*it)->linha << endl
								 << "Posicao: " << (*it)->posicao << endl
								 << "-----------------------------" << endl << endl;

							break; // só existe uma palavra seguida da outra, as proximas nao tem como ser
						}
					}
				}
			}
		}
		return true;
	}
	comp++;
	if(p1 < (*raiz)->palavra) // busca para esquera
		return buscar(&((*raiz)->esq), d1, p1, p2);

	return buscar(&((*raiz)->dir), d1, p1, p2); // busca para direita
}

bool buscar (no **raiz1, no **raiz2, const string &p1, const string &p2, int comp) // busca 1 para duas palavras
{	
	comp++;
	if((*raiz1) == NULL) // nao existe a palavra, logo nao existe a sequencia de p1 e p2 tambem
		return false;
	comp++;
	if(p1 == (*raiz1)->palavra)// achou p1 e deve procurar p2
		return buscar (raiz2, (*raiz1)->dado, p1, p2);
	comp++;
	if(p1 < (*raiz1)->palavra)// busca para esquerda
		return buscar(&((*raiz1)->esq), raiz2, p1, p2);

	return buscar(&((*raiz1)->dir), raiz2, p1, p2); // busca para direita
}

void deletar (no **raiz)
{
	if((*raiz) == NULL)
		return;

	deletar(&((*raiz)->esq));
	deletar(&((*raiz)->dir));

	delete (*raiz);
}


int indicie (const char &c)
{
	// tabela ascii
	if(c >= 65 && c <= 90)
		return (int)(c - 65);
	if(c >= 97 && c <= 122)
		return (int)(c - 97);

	return 26;
}

void insere (Hash *h, const string &palavra, Dados *dado, long &cont)
{
	inserir(&(h[indicie(palavra[0])].arvore), palavra, dado, cont);
}

bool buscar_hash (Hash *h, const string &palavra, long &comp)
{
	return buscar(&(h[indicie(palavra[0])].arvore), palavra, comp);
}

void deletar(Hash *h)
{
	for(int i = 0; i < ALFABETO; i++)
		deletar(&(h[i].arvore));

	delete h;
}

string nome_arq(string nome, const int n)
{
	stringstream to_string;


	if(n < 10)
	{
		to_string << n;
		nome = "000" + to_string.str() + ".txt";
		return nome;
	}

	if(n < 100)
	{
		to_string << n;
		nome = "00" + to_string.str() + ".txt";
		return nome;
	}

	if(n < 1000)
	{
		to_string << n;
		nome = "0" + to_string.str() + ".txt";
		return nome;
	}

	to_string << n;
	to_string.str() + ".txt";
	return nome;
}

Hash* inicializa_hash(){

	Hash* h;
	h = new Hash[ALFABETO];
	for(int i = 0; i < ALFABETO; i++){
		h[i].arvore = iniciar_arvore();
	}
	return h;
}

void arquivo_invertido(string padrao1, string padrao2, int num_padroes, bool imprime)
{
	
	/*estruturas utilizadas*/
	Hash *h;
	dados *dado;

	h = inicializa_hash();//inicializacao do Hash

	ifstream arq;//variavel arquivo

	string linha;//guardara a linha lida 

	/*para os dados das ocorencias*/
	string palavra;
	int posicao, lin;
	string nome_documento;
	
	clock_t tempo_execucao = 0; // variavel para contar o tempo
	clock_t tempo_pre_procesamento = 0; // variavel para contar o tempo

	long comp = 0;//conta comparacoes de busaca pos pre processamento
	long cont = 0;//conta as comparacoes de pre processamneto

	/*referente a leitura dos 1000 arquivos texto*/
	tempo_pre_procesamento = clock();//conta o tempo de pre-processamento
	for(int i = 1; i <= 1000; i++)
	{
	    nome_documento = nome_arq(nome_documento, i);//gera o nome do arquivo a ser aberto
		arq.open(nome_documento.data());//abertura do arquivo

		/*validacao*/
		if(arq.fail())
		{
			cout << "Falha ao abrir o arquivo " << i << endl;
			exit(1);
		}

		lin = 1;

		/*leitura*/
		while(!arq.eof())
		{
			posicao = 0;

			getline(arq, linha);//le a linha 
			stringstream str(linha);

			while(str >> palavra)//divide a linha em palavras
			{
				posicao += palavra.size() + 1;//seta o dado posicao
				dado = iniciar_dados(lin, posicao, i);//seta os dados da ocorrencia da palavra
				insere(h, palavra, dado, cont);//insere na estrutura hash
			}

			lin++;//incremeta as linhas lidas
		}

		arq.close();//fecha o arquivo
	}
	tempo_pre_procesamento = clock() - tempo_pre_procesamento;
	tempo_pre_procesamento = ((float)tempo_pre_procesamento) * 1000.0 / CLOCKS_PER_SEC;


	/*calcula o tempo de execucao*/
	tempo_execucao = clock();

	/*realiza a busaca, em caso de falha imprime o aviso*/
	if(num_padroes == 1)
		if(buscar_hash(h, padrao1, comp) == false){
		cout << "palavra não encontrada"<< endl;
		}
	else if(num_padroes == 2)
		if(buscar(&(h[indicie(padrao1[0])].arvore), &(h[indicie(padrao2[0])].arvore), padrao1, padrao2)){
		cout << "palavra não encontrada"<< endl;
		}
	tempo_execucao = clock() - tempo_execucao;
	tempo_execucao = ((float)tempo_execucao) * 1000.0 / CLOCKS_PER_SEC;

	/*se especificado, imprime os dados de tempo*/
	if(imprime == true){
		cout << "\nTempo = "<< tempo_execucao << "ms "<< endl;
		cout << "Comparacoes = "<< comp << endl;
	}

	cout << "\nTempo de pre-processamneto = "<< tempo_pre_procesamento << endl;
	cout << "Comparacoes em pre-processamneto = "<< comp << endl;

	//deletar(h);//libera a estrutura do hash
	
}
/*
int main()
{

	string padrao;

	cout << "Digite o padrao : ";
	cin >> padrao;

	arquivo_invertido(padrao, true);
}*/