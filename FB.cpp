#include <iostream>
#include <string>
#include <string.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

#include "FB.hpp"
#include "Arquivo_invertido.hpp"
using namespace std;


struct estatisticas{
    long comparacoes;
    long deslocamentos;
};

void forcaBruta(string T, long n,string  P,long m,Estatisticas &e){
    long i,j,k,h=0;
    for(i=1;i<=(n-m+1);i++){
        e.deslocamentos++;
        k=i;
        j=1;
        e.comparacoes++;
        while(T[k-1]==P[j-1]&& j <= m){
            e.comparacoes++;
            j++;
            k++;
        }
        if(j>m){
            cout << "Casamento "<< h+1 <<" comeca em: " << i << endl;
            h++;//conta os casamentos
        }
    }
    if(h == 0){//se nao houver casamento 
    	cout << "Nao houve casamento com o padrao '"<< P <<"'"<< endl;
    }
}

/*recebe o numero do arquivo a ser aberto
  e executa o metodo forca bruta*/
void forca_Bruta(string padrao, bool imprime){

    string texto;//guarda o texto
    ifstream arq;
    Estatisticas e;

    clock_t tempo_execucao = 0; // variavel para contar o tempo

    /*inicializa os contadores */
    e.comparacoes = 0;
    e.deslocamentos = 0;

    /*nome logico para abertura do arquivo*/
    string nome;

    string aux;//auxiliar para a leitura do txt

    for(int i=1; i<=1000; i++){//abre todos os aquivos
        
        texto = "";
        nome = nome_arq(nome, i);//gera o nome a partir do numero desejado
        /*abertura e validacao*/
        arq.open(nome.data());
        if(arq == NULL){
            cout << "Erro na abertura "<< endl;
            exit(1);
        }

        /*leiura*/
        while (arq.eof() == false) {
            getline(arq, aux);
            texto += aux; // concatena as strings corretamente até acabar o arquivo
        }
        arq.close();
        cout << endl;
        cout << "RESULTADOS PARA O ARQUIVO ["<< i <<"]"<< endl;

        tempo_execucao = clock();
        forcaBruta(texto, texto.size(), padrao, padrao.size(), e);
        tempo_execucao = clock() - tempo_execucao;
        tempo_execucao = ((float)tempo_execucao) * 1000.0 / CLOCKS_PER_SEC;
        /*cout << "TEXTO "<< endl;
        cout << texto << endl;*/
  
    }
    if(imprime == true){
        cout << endl;
        cout << "\nComp = "<< e.comparacoes << endl;
        cout << "Desl = "<< e. deslocamentos << endl;
        cout << "Tempo = "<< tempo_execucao << "ms "<< endl;
    }

}
/*
int main(){


    string padrao;
    cout << "Digite  o padrao: ";
    cin >> padrao;

    forca_Bruta(padrao, true);
    return 0;
}*/