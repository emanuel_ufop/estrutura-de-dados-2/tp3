#include<iostream>
#include<stdlib.h>
#include<stdio.h>
#include <fstream>
#include <sstream>
#include <string>

#include "Arquivo_invertido.hpp"
#include "FB.hpp"

using namespace std;

int main(int argc, char* argv[]){

	if((argc < 5 || argc > 5) || !(toupper(*argv[3]) == 'S' || toupper(*argv[3]) == 'N')){// se os parametros forem passaodos erroneamente 
		cout <<"Formato de execucao errado "<< endl;
		cout <<"Formato de execucao : <método> <padrao> (impressao[S/N]) "<< endl;
		cout <<"Formato de execucao : <método> <padrao1> <padrao2> (impressao[S/N]) "<< endl;
		exit(1);

	}


	int metodo = atoi(argv[1]);
	string padrao1 = argv[2];
	string padrao2 = argv[3];
	bool imprime = false;

	if (toupper(*argv[3])== 'S'){// imprime os dados do registro buscado
		imprime = true;
	}	

	//cout << "padrao1 = "<< argv[2]<< endl;

	switch(metodo){

		case 1:{//ARQUIVO INVERTIDO
			if(argc == 4)
				arquivo_invertido(padrao1, padrao2, 0, imprime);
			if(argc == 5)
				arquivo_invertido(padrao1, padrao2, 1, imprime);
		}
		break;
		case 2:{//FORCA BRUTA
			forca_Bruta(padrao1, imprime);
		}
		break;

	}//fim do switch

	return 0;
}