#include <iostream>
#include <string>
#include <string.h>
#include <list>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

#ifndef Arquivo_invertido_hpp
#define Arquivo_invertido_hpp


typedef struct dados Dados;
typedef struct no no;
typedef struct hash Hash;

no* iniciar_arvore();
no* no_cria(string p);
dados* iniciar_dados (const int &linha, const int &posicao, const int &arquivo);
void inserir (no **raiz, const string &palavra, Dados *dado, long &cont);
void imprimir(list<dados*>& lista);
bool buscar (no **raiz, const string &palavra, long &comp);
bool buscar (no **raiz, list<Dados*> &d1, const string &p1, const string &p2, int comp);
bool buscar (no **raiz1, no **raiz2, const string &p1, const string &p2, int comp);
void deletar (no **raiz);

int indicie (const char &c);
void insere (Hash *h, const string &palavra, Dados *dado, long &cont);
bool buscar_hash (Hash *h, const string &palavra, long &comp);
void deletar(Hash *h);
string nome_arq (string nome, const int n);
Hash* inicializa_hash();

void arquivo_invertido(string padrao, bool imprime);



#endif